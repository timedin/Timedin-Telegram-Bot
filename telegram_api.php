<?php
$baseURI = "https://api.telegram.org/YOUR_BOT_KEY/";
$senderAPI = "https://api.example.com/telegram/";

require_once($_SERVER["RESOURCES_PATH"]."/php/DBManager.php");
$config = parse_ini_file('./mysql.ini');
$DBMngr = new DBManager($config["host"], $config["user"],$config["pass"], $config["dbname"]);


$json = file_get_contents("php://input");
$input = json_decode($json,true);
if(($input??null) ==null) return;
$array = $input["message"];
$message = explode(" ", $array["text"]);
$cmd =  $message[0];
$args = array_slice($message, 1);
function postCurl($url,$value){
	$req = curl_init($url);
	curl_setopt($req, CURLOPT_POST, 1);
	curl_setopt($req, CURLOPT_POSTFIELDS, $value);
	curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($req, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
	$out = curl_exec($req);
	curl_close($req);
	return $out;
}

switch (strtolower($cmd)) {
	case "/register":
		switch (strtolower($args[0]??null)) {
			case "custom":
				if($args[1]== null) {
					postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"Please set a name for the custom connection."}');
				} else {
					$sql = "INSERT INTO `entries` (`id`, `chat_id`, `custom_name`) VALUES (\"%a0\", \"%a1\", \"%a2\");";
					if(strlen($args[1])>80) {
						postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"Please use a shorter name!"}');
						die();
					}
					if($DBMngr->QuerySingleRow("SELECT * FROM entries WHERE `chat_id` = \"%a0\" AND `custom_name` = \"%a1\";",$array['chat']['id'],$args[1])!=null) {
						postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"This custom name already exists."}');
						die();
					}
					$id = substr(md5(rand()),0,20);
					
					$DBMngr->insert($sql,strval($id),strval($array["chat"]["id"]),$args[1]);
					postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"You can now send notifications from me with {$senderAPI}/telegram/send?id=${id}&msg=[your_message]","disable_web_page_preview": true}');
				}
				break;
			case null: case " ":
			  postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"Oops! You didn\'t specify a register-category."');
			  break;
			default:
				postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"Oops! I don\'t know the register-category."');
		}
		break;
	case "/help": case "/start": case "hello":
		postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"👋"}');
		postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"Hey you! \n\nI can send you notifications from Timedin.net. \n\n You can register by using: \n /register [event name] [token]\n Or unregister by using: \n /stop [event name/all]"}');
		break;
	case "/list":
		$msg = "You registered for the following services: \n";
		$sql = 'SELECT custom_name FROM `entries` WHERE  `chat_id` = "%a0" AND `custom_name` != "";';
		$res = $DBMngr->query($sql,$array["chat"]["id"]);
		if($res == null) {
			$msg = "You registered for no services...";
			postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"${msg}"}');
			die();
		}
		foreach($DBMngr->query($sql,$array["chat"]["id"]) as $items) {
				$msg = $msg."\n-".$items["custom_name"];
		}
		$msg = $msg . "\n\n Unsubscribe with /stop [name]";
		postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"${msg}"}');
		break;
	case "/stop":
		switch(strtolower($args[0]??null)) {
			case "all":
				$sql = "DELETE FROM `entries`WHERE `chat-id`=\"%a0\";";
				if($DBMngr->delete($sql,strval($array["chat"]["id"]))) {
					postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"Unsubscribing from all notifications!"}');
				} else {
					postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"You\'r not subscribed to any notifications!"}');
				}
				break;
			case "custom":
				$sql = "DELETE FROM `entries`WHERE `chat_id`=\"%a0\";";
				if($DBMngr->delete($sql,strval($array["chat"]["id"]))) {
					postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"Unsubscribing from all custom notifications!"}');
				} else {
					postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"You\'r not subscribed to any notifications!"}');
				}
				break;
			case null: case " ":
			  postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"Oops! You didn\'t specify a register-category."');
			  break;
			default:
				$sql = 'SELECT custom_name FROM `entries` WHERE  `chat_id` = "%a0" AND `custom_name` = "%a1"';
				if($DBMngr->query($sql,$array["chat"]["id"],$args[0]) !=null) {
					$sql = "DELETE FROM `entries`WHERE `chat_id`=\"%a0\" AND `custom_name` = \"%a1\";";
					$DBMngr->delete($sql,strval($array["chat"]["id"]),$args[0]);
					postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"Deleted your entry."');
					die();
				} else {
					postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"Oops! I don\'t know the specified custom connection."');
					die();
				}
		}
		break;
	default:
		postCurl($baseURI."sendMessage",'{"chat_id":${array["chat"]["id"]},"text":"Oops! I don\'t understand your message. Use /help to learn more about me and my commands!"}');
}

?>
