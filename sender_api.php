<?php
$baseURI = "https://api.telegram.org/YOUR_BOT_KEY/";

require_once($_SERVER["RESOURCES_PATH"]."/php/DBManager.php");
$config = parse_ini_file('./mysql.ini');
$DBMngr = new DBManager($config["host"], $config["user"],$config["pass"], $config["dbname"]);


function postCurl($url,$value){
	$req = curl_init($url);
	curl_setopt($req, CURLOPT_POST, 1);
	curl_setopt($req, CURLOPT_POSTFIELDS, $value);
	curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($req, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
	$out = curl_exec($req);
	curl_close($req);
	return $out;
}
header("Content-Type: application/json");
if(isset($_GET["msg"])&&isset($_GET["id"])) {
	$sql = "SELECT `chat_id`,`custom_name` FROM `entries` WHERE `id` = \"%a0\";";
	$db_Array=$DBMngr->QuerySingleRow($sql,$_GET["id"]);
	if($db_Array == null) {
		http_response_code(400);
		die('{"state":"error","description":"Sender_ID not found!"}');
	}
	$msg = $_GET["msg"];
	postCurl($baseURI."sendMessage",json_encode(array('chat_id' => $db_Array["chat_id"],"text"=> "[Custom: \"{$db_Array["custom_name"]}\"]: {$msg}")));
	die('{"state":"success"}');
} else {
	http_response_code(400);
	die('{"state":"error","description":"missing parameters!"}');
}
?>
